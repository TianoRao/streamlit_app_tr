# Streamlit Chatbot with Hugging Face Integration
Tianji Rao
## About

This project showcases a Streamlit-based web application that serves as a chat interface with a Hugging Face Large Language Model (LLM). Designed to demonstrate the integration of advanced AI models within web applications, this chatbot aims to provide users with interactive and insightful conversations, powered by the latest advancements in natural language processing.

## Features

- **Interactive Chat Interface**: Engage in conversations with a chatbot powered by Hugging Face's LLM.
- **Responsive Web Design**: A user-friendly website that's accessible on both desktop and mobile devices.
- **Deployment Options**: Instructions for deploying both locally and on an Amazon EC2 instance for wider accessibility.


## Installation and Setup

### Prerequisites

- Python 3.8 or higher
- pip
- An AWS account (for EC2 deployment)

### Local Deployment

1. **Clone the repository:**
   ```bash
   git clone https://github.com/yourusername/yourprojectname.git
   cd yourprojectname
   ```

2. **Install the requirements:**
   ```bash
   pip install -r requirements.txt
   ```

3. **Run the Streamlit app:**
   ```bash
   python3 -m streamlit run app.py
   ```

### EC2 Deployment

Follow the detailed instructions in the [youtube link](https://www.youtube.com/watch?v=DflWqmppOAg) to deploy your Streamlit app on an Amazon EC2 instance.

## Usage

Once the application is running, navigate to the provided URL (e.g., http://localhost:8501 for local deployment) to start interacting with the chatbot.

## Link

[Streamlit Link](https://chatbottt.streamlit.app/)

[EC2 Link](https://54.227.25.70:8501)

## Screenshots
![alt text](imgs/image.png)

![alt text](imgs/image-1.png)
